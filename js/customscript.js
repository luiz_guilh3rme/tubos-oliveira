jQuery( document ).ready( function ( $ ) {
    'use strict';
    // Menu fixes
    $( function () {
        if ( $( window ).width() > 767 ) {
            $( ".dropdown, .dropdown-submenu" ).hover(
                function () {
                    $( this ).addClass( 'open' )
                },
                function () {
                    $( this ).removeClass( 'open' )
                }
            );
        }
    } );
    $( '.navbar .dropdown-toggle' ).hover( function () {
        $( this ).addClass( 'disabled' );
    } );
    
    $( window ).scroll( function () {
        var $unstick = $( 'body.unstick-menu' );
        if ( !$unstick.length ) {
            var topmenu = $( '#top-navigation' ).outerHeight();
            var header = $( '.site-header' ).outerHeight();
            if ( $( document ).scrollTop() > ( topmenu + header + 50 ) ) {
                $( 'nav#site-navigation' ).addClass( 'shrink' );
            } else {
                $( 'nav#site-navigation' ).removeClass( 'shrink' );
            }
        }
    } );

    var $myDiv = $( '#futurio-floating' );
    if ( $myDiv.length ) {
        $( window ).scroll( function () {
            var distanceTop = $myDiv.prev().position().top + $myDiv.prev().height() + 80;

            if ( $( document ).scrollTop() > distanceTop ) {
                $myDiv.addClass( 'floating-element' );
            } else {
                $myDiv.removeClass( 'floating-element' );
            }
        } );
    }
    
    // Mobile menu function
    function FuturioMobileMenu() {
        $( '.open-panel' ).each( function () {
            var menu = $( this ).data( 'panel' );
            if ( $( window ).width() < 768 ) {
                $( "#" + menu ).click( function ( e ) {
                     e.preventDefault();
                    $( "#blog" ).toggleClass( "openNav" );
                    $( "#" + menu + ".open-panel" ).toggleClass( "open" );
                } );
                $( "#site-navigation .menu-container a" ).click( function () {
                    $( "#blog" ).toggleClass( "openNav" );
                    $( "#" + menu + ".open-panel" ).toggleClass( "open" );
                } );
            }
        } );
    }
    var $openPanel = $( '.open-panel' );
    if ( $openPanel.length ) {
        // Fire mobile menu
        FuturioMobileMenu();
    }

    $( '.top-search-icon .fa' ).click( function () {
        $( ".top-search-box" ).toggle( 'slow' );
        $( ".top-search-icon .fa" ).toggleClass( "fa-times fa-search" );
    } );

    $( '.offcanvas-sidebar-toggle' ).on( 'click', function () {
        $( 'body' ).toggleClass( 'offcanvas-sidebar-expanded' );
    } );
    $( '.offcanvas-sidebar-close' ).on( 'click', function () {
        $( 'body' ).toggleClass( 'offcanvas-sidebar-expanded' );
    } );

    $( 'body' ).addClass( 'loaded' );

    var sections = $( 'section' )
        , nav = $( '#site-navigation.navbar' )
        , nav_height = nav.outerHeight();

    $( window ).on( 'scroll', function () {
        var cur_pos = $( this ).scrollTop();

        sections.each( function () {
            var top = $( this ).offset().top - nav_height - 30,
                bottom = top + $( this ).outerHeight();

            if ( cur_pos >= top && cur_pos <= bottom ) {
                nav.find( 'a' ).parent().removeClass( 'active' );
                sections.removeClass( 'active' );

                $( this ).addClass( 'active' );
                nav.find( 'a[href="#' + $( this ).attr( 'id' ) + '"]' ).parent().addClass( 'active' );
            }
        } );
    } );

    $( document ).on( 'click', '.button.ajax_add_to_cart', function ( e ) {
        e.preventDefault();
        $( 'body.open-head-cart' ).addClass( 'product-added-to-cart' );
    } );
    $( document ).on( 'mouseover', '.product-added-to-cart ul.site-header-cart', function ( e ) {
        e.preventDefault();
        $( 'body.open-head-cart' ).removeClass( 'product-added-to-cart' );
    } );

//phonecta

function openPhoneCta() {
    // duhh
    $('.confirm, .open-modal').on('click', function () {
        $('.cta-overlay').fadeIn(400, function () {
            $('.form-wrapper-all').fadeIn();
        });
   });
}

function closeModalCta() {
    $('.close-modal').on('click', function () {
        $('.form-wrapper-all').fadeOut(400, function () {
            $('.cta-overlay').fadeOut();
               $('body, html').css('overflow-y','initial');
               $('body, html').css('overflow-x', 'hidden');
        });
    });
}

function closeCTA() {
    $('.cta-tooltip').addClass('clicked');
}

function reopenCTA() {
    $('.cta-tooltip').removeClass('clicked');
}

function formPickers() {
    $('.form-pickers').removeClass('active');
    $(this).addClass('active');
    var instance = $(this).data('instance');

    $('.instance:not([data-instance=' + instance + '])').fadeOut(400, function () {
        setTimeout(function () {
            $('.instance[data-instance=' + instance + ']').fadeIn()
        }, 400)
    });
}

function randomizeRequests(min, max) {
    var random = Math.floor(Math.random() * (max - min + 1)) + min;
    return random;
}

function printNumbers() {
    var numbers = document.querySelectorAll('.number');
    for (var i = 0; i < numbers.length; i++) {
        numbers[i].innerHTML = randomizeRequests(1, 12);
    }
}

openPhoneCta();
closeModalCta();
printNumbers();
$('.form-pickers').on('click', formPickers);
$('.close-cta').on('click', closeCTA); 
$('.phone-icon').on('click', reopenCTA);




    $('input[name="phone"]').mask('(00) 0 0000-0000');
    $('input[name="data"]').mask('00/00/0000');
    $('input[name="hora"').mask('00h00');


    //desk
    $('.confirm').click(function(e){
          e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
              $('body, html').css('overflow','hidden');  
    });
    //mobile
    $('.open-modal').click(function(e){
      e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
            $('body, html').css('overflow-y','initial');
            $('.outline-menu').fadeOut('fast');
            $('.btn-menu').removeClass('change');
           
          $('body, html').css('overflow','hidden');  

    });



} );

