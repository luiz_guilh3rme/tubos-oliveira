		<div class="call-cta-wrapper">
		<div class="cta-tooltip">
			<p class="tooltip-text">
				<p class="main-txt">Olá!</p>
				<p class="desc">Gostaria de receber uma ligação?</p>
			</p>
			<button class="confirm"><p>SIM</p></button>
			<button class="close-cta" aria-label="Fechar CTA de whatsapp">
				&times;
			</button>
		</div>
		<a target="_blank" class="whats-desktop" href="wpp">
			<button class="whats-icon">
				<img src="<?php echo get_template_directory_uri(). '/img/whatsapp.png'; ?>" alt="Whatsapp">
			</button>
		</a>
		<button class="phone-icon">
			<i class="fa fa-phone"></i>
		</button>
	</div>
	<div class="cta-overlay">
		<button class="close-modal close-cta-forms" aria-label="Fechar Modal">
			&times;
		</button>
		
	<!-- 	<div class="bottom-text">
			<p>Powered by:</p>
			<img src="<?php //echo get_template_directory_uri(). '/img/3xceler-logo.png'; ?>" alt="Logo 3xceler">
		</div> -->
		<div class="form-wrapper-all">
			<div class="form-picker">
				<button class="form-pickers" data-instance="00">
				<i class="fa fa-phone"></i>
					<p>ME LIGUE AGORA</p>
				</button>
				<button class="form-pickers" data-instance="01">
				<i class="fa fa-clock-o alt"></i>
					<p>ME LIGUE DEPOIS</p>
				</button>
				<button class="form-pickers active" data-instance="02">
				<i class="fa fa-comments alt"></i>
					<p>DEIXE UMA MENSAGEM</p>
				</button>
			</div>
			<div class="instance" data-instance="00">
				<div class="leave-message">
					<legend class="leave-title">
						<span class="variant">
							NÓS TE LIGAMOS!
						</span>
						Informe seu telefone que entraremos em
						contato o mais rápido possível.
					</legend>
					<div class="pop-box">
					<?php echo do_shortcode( '[contact-form-7 id="1872" title="Box - Me ligue agora"]' ); ?>
				</div>
					<div class="fields cleared">
						<p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
					</div>
				</div>
			</div>
			<div class="instance" data-instance="01">
				<div class="leave-message schedule-time">
					<legend class="leave-title">
						Gostaria de agendar e receber uma
						chamada em outro horário?
					</legend>
				<div class="pop-box">
					<?php echo do_shortcode( '[contact-form-7 id="1871" title="Box - Me ligue depois"]' ); ?>
				</div>
					<div class="fields cleared">
						<p class="callers">Você já é a <span class="number">5</span> pessoa a solicitar uma ligação.</p>
					</div>
				</div>
			</div>
			<div class="instance active" data-instance="02">
				<div class="leave-message">
					<legend class="leave-title">
						Deixe sua mensagem! Entraremos em
						contato o mais rápido possível.
					</legend>
					<div class="pop-box">
							<?php echo do_shortcode( '[contact-form-7 id="1870" title="Box - Deixe uma mensagem"]' ); ?>
					</div>
					<div class="fields cleared">
						<p class="callers">Você já é a <span class="number">3</span> pessoa a deixar uma mensagem.</p>
					</div>
				</div>
			</div>
		</div>
	</div>