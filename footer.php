</div><!-- end main-container -->
</div><!-- end page-area -->
<?php if ( is_active_sidebar( 'futurio-footer-area' ) && futurio_get_meta( 'disable_footer_widgets' ) != 'disable' ) { ?>  				
	<div id="content-footer-section" class="container-fluid clearfix">
		<div class="container">
			<?php dynamic_sidebar( 'futurio-footer-area' ); ?>
		</div>	
	</div>		
<?php } ?>

<?php do_action( 'futurio_before_footer' ); ?>

<?php
if ( futurio_get_meta( 'disable_footer' ) != 'disable' ) {
	do_action( 'futurio_generate_footer' );
}
?>

</div><!-- end page-wrap -->

<?php do_action( 'futurio_after_footer' ); ?>

<?php if ( is_active_sidebar( 'futurio-menu-area' ) ) { ?>
	<div id="site-menu-sidebar" class="offcanvas-sidebar" >
		<div class="offcanvas-sidebar-close">
			<i class="fa fa-times"></i>
		</div>
		<?php dynamic_sidebar( 'futurio-menu-area' ); ?>
	</div>
<?php } ?>

<?php 
	include 'template-parts/cta-wrapper.php';
	include 'template-parts/mobile-cta-wrapper.php';
 ?>


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9KJBTX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php wp_footer(); ?>

</body>
</html>
